updateLocalizations:
	for domain in $$( ls `pwd`/src/nfo_tool/locales/*.pot ); do domain_name=$$( basename $${domain/.pot/} ); for lang in $$( find `pwd`/src/nfo_tool/locales/ -mindepth 1 -maxdepth 1 -type d -exec basename {} \; ); do msgmerge --update `pwd`/src/nfo_tool/locales/$$lang/LC_MESSAGES/$$domain_name.po $$domain; done; done

addLocalization:
	mkdir -p `pwd`/src/nfo_tool/locales/$(LANG)/LC_MESSAGES \
	&& for domain in $$( ls `pwd`/src/nfo_tool/locales/*.pot ); do domain_name=$$( basename $${domain/.pot/} ); msginit --input=$$domain --output=`pwd`/src/nfo_tool/locales/$(LANG)/LC_MESSAGES/$$domain_name.po --locale=$(LANG); done

buildLocalizations:
	for domain in $$( ls `pwd`/src/nfo_tool/locales/*.pot ); do domain_name=$$( basename $${domain/.pot/} ); for lang in $$( find `pwd`/src/nfo_tool/locales/ -mindepth 1 -maxdepth 1 -type d -exec basename {} \; ); do msgfmt --output=`pwd`/src/nfo_tool/locales/$$lang/LC_MESSAGES/$$domain_name.mo `pwd`/src/nfo_tool/locales/$$lang/LC_MESSAGES/$$domain_name.po; done; done
