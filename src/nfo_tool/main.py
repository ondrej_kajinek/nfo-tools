"""Video analyzer

Analyze all video files in given directory tree and creates
`.nfo` files for them.

Definition of NFO can be found at https://kodi.wiki/view/NFO_files/Movies
"""

import argparse
import gettext
import logging
import pathlib

from nfo_tool.nfo_commands import NFO_COMMANDS


log = logging.getLogger(__file__)


LOCALES_DIR = pathlib.Path(__file__).parent.resolve() / "locales"


def main():
    """Application main function."""
    cmd_args = _get_cmd_args()
    _initialize_gettext()
    command = NFO_COMMANDS[cmd_args.command]
    try:
        command(cmd_args.directory)
    except KeyboardInterrupt:
        log.error("Interrupted by user.")


def _check_cmd_args(cmd_args) -> None:
    if isinstance(cmd_args.directory, str):
        cmd_args.directory = pathlib.Path(cmd_args.directory)

    if not pathlib.Path(cmd_args.directory).is_dir():
        raise RuntimeError("Given directory doesn't exist!")


def _get_cmd_args():
    parser = argparse.ArgumentParser(
        description="Tool for creating .nfo files for video library"
    )
    parser.add_argument(
        "command",
        choices=tuple(NFO_COMMANDS.keys()),
        help="NFO command",
    )
    parser.add_argument(
        "directory",
        nargs="?",
        default=pathlib.Path().cwd(),
        help="Directory to search for video files",
    )
    cmd_args = parser.parse_args()
    _check_cmd_args(cmd_args)
    return cmd_args


def _initialize_gettext():
    for domain in ("country", "genre"):
        gettext.bindtextdomain(domain, LOCALES_DIR)


if __name__ == "__main__":
    main()
