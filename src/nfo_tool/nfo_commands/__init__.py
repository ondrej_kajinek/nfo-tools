from nfo_tool.nfo_commands.remove import remove
from nfo_tool.nfo_commands.update import update

NFO_COMMANDS = {
    method.__name__: method
    for method in (update, remove)
}

__all__ = ["NFO_COMMANDS"]
