import logging
import pathlib
import traceback

import ffmpeg
import filelock

from nfo_tool import imdb_utils
from nfo_tool.nfo_commands.utils import find_video_files
from nfo_tool.video_metadata import Metadata


log = logging.getLogger(__file__)


def update(directory: pathlib.Path) -> None:
    """Create .nfo files for video files.

    Iterate over all video files in :py:variable:`directory` subtree
    and create / update .nfo files when missing or being older that
    the movie file.

    Parameters
    ----------
    directory : pathlib.Path
        Directory to search for video files
    """
    for video_file in find_video_files(directory):
        nfo_file = video_file.with_suffix(".nfo")
        lock_file = video_file.with_suffix(".nfo.lock")
        lock = filelock.FileLock(lock_file)
        with lock:
            print(f"lock on {nfo_file!s} acquired")
            if not (
                nfo_file.is_file() and
                video_file.stat().st_mtime < nfo_file.stat().st_mtime
            ):
                try:
                    _create_nfo(video_file, nfo_file)
                except RuntimeError as exc:
                    log.error("Failed when creating %s: %s", nfo_file, exc)
                except BaseException as exc:
                    log.error(
                        "Exception when creating %s: %s: %s",
                        nfo_file, exc, traceback.format_exc()
                    )
                else:
                    log.info("%s successfully created", nfo_file)
            else:
                log.info("%s already exists and is up-to-date", nfo_file)

        lock_file.unlink(missing_ok=True)


def _create_nfo(video_file: pathlib.Path, nfo_file: pathlib.Path) -> None:
    try:
        detail = imdb_utils.get_info(video_file.stem)
    except imdb_utils.ImdbError as exc:
        log.warning("%s, creating empty detail", exc)
        detail = imdb_utils.ImdbDetail()

    probed_data = ffmpeg.probe(str(video_file))
    probed_data["streams"] = {
        codec_type: [
            stream
            for stream in probed_data["streams"]
            if stream["codec_type"] == codec_type
        ]
        for codec_type in ("video", "audio", "subtitle")
    }
    metadata = Metadata(detail, probed_data)
    metadata.save(nfo_file)
