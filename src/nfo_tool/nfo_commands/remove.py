import pathlib

from nfo_tool.nfo_commands.utils import find_video_files


def remove(directory: pathlib.Path) -> None:
    """Remove .nfo files in directory subtree.

    Parameters
    ----------
    directory :
        Starting directory to search for video files
    """
    for nfo_file in map(
            lambda x: x.with_suffix(".nfo"),
            find_video_files(directory)
    ):
        try:
            nfo_file.unlink()
        except (IsADirectoryError, FileNotFoundError):
            pass
