import mimetypes
import pathlib
import typing as t


def find_video_files(directory: pathlib.Path) -> t.Iterator[pathlib.Path]:
    """Find video files in directory subtree.

    Parameters
    ----------
    directory : pathlib.Path
        Directory to be recursively searched for video files.

    Yields
    ------
    pathlib.Path:
        Video file absolute path
    """
    dirs = [directory.resolve()]
    while dirs:
        current_directory = dirs.pop()
        for item in current_directory.iterdir():
            if item.is_dir():
                dirs.append(item)
            elif _is_video_file(item):
                yield item


def _is_video_file(file_: pathlib.Path) -> bool:
    """Check if file contains video data.

    Parameters
    ----------
    file_: pathlib.Path
        File to be checked

    Returns
    -------
    is_video: bool
        `True` if :param:`file_` contains video data, `False` otherwise.
    """
    mime_type = mimetypes.guess_type(file_)[0] or ""
    return mime_type.split("/", 1)[0] == "video"
