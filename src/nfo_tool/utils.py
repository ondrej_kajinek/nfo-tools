import datetime


def parse_date(date_str: str) -> datetime.datetime:
    """Parse date string.

    Parameters
    ----------
    date_str : str
        Date string

    Returns
    -------
    parsed : datetime.datetime
        Parsed datetime
    """
    try:
        parsed = datetime.datetime.strptime(date_str, "%d %b %Y")
    except ValueError:
        try:
            parsed = datetime.datetime.strptime(date_str, "%b %Y")
        except ValueError:
            parsed = datetime.datetime.strptime(date_str, "%Y")

    return parsed
