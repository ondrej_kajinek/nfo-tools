import fractions
import gettext
import typing as t

import yattag


def get_aspect_ratio(stream: t.Dict) -> str:
    """Get aspect ratio of video stream.

    Parameters
    ----------
    stream : dict
        Stream data

    Returns
    -------
    aspect_ratio : str
        Aspect ratio represented as "{width}:{height}" format
    """
    try:
        aspect_ratio = stream["display_aspect_ratio"]
    except KeyError:
        ratio = fractions.Fraction(stream["width"], stream["height"])
        aspect_ratio = f"{ratio.numerator}:{ratio.denominator}"

    return aspect_ratio


class Metadata:

    def __init__(self, imdb_detail, stream_data):
        self.imdb_detail = imdb_detail
        self.streams = stream_data["streams"]
        self.format = stream_data["format"]

    def save(self, xml_target):
        """Save video metadata to xml file."""
        doc, tag, text, line = yattag.Doc().ttl()
        doc.asis("<?xml version='1.0' encoding='UTF-8'?>")
        with tag("movie"):
            line("title", self.imdb_detail.title)

            if self.imdb_detail.original_title:
                line("originaltitle", self.imdb_detail.original_title)

            line("runtime", "")
            line(
                "uniqueid",
                f"tt{self.imdb_detail.imdb_id}",
                type="imdb",
                default="true",
            )
            for genre in self.imdb_detail.genres:
                line("genre", gettext.dgettext("genre", genre))

            for country in self.imdb_detail.countries:
                line("country", gettext.dgettext("country", country))

            for writer in self.imdb_detail.writers:
                line("credits", writer)

            for director in self.imdb_detail.directors:
                line("director", director)

            if self.imdb_detail.release_date:
                line(
                    "premiered",
                    self.imdb_detail.release_date.strftime("%Y-%m-%d"),
                )

            with tag("fileinfo"):
                with tag("streamdetails"):
                    for stream in self.streams["video"]:
                        with tag("video"):
                            line("codec", stream["codec_name"])
                            line("aspect", get_aspect_ratio(stream))
                            line("width", stream["width"])
                            line("height", stream["height"])
                            line(
                                "durationinseconds",
                                (
                                    stream.get("duration") or
                                    int(float(self.format["duration"]))
                                ),
                            )
                            # line("stereomode", stream["codec_name"])

                    for stream in self.streams["audio"]:
                        try:
                            language = stream["tags"]["language"]
                        except KeyError:
                            language = "und"

                        with tag("audio"):
                            line("codec", stream["codec_name"])
                            line("language", language)
                            line("channels", stream["channels"])

                    for stream in self.streams["subtitle"]:
                        try:
                            language = stream["tags"]["language"]
                        except KeyError:
                            language = "und"

                        with tag("subtitle"):
                            line("language", language)

        xml_target.write_text(yattag.indent(doc.getvalue()))
