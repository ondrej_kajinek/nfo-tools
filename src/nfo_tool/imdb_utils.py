import re
from dataclasses import dataclass, field

from imdb import IMDb

from nfo_tool.utils import parse_date


@dataclass
class ImdbDetail:
    imdb_id: str = None
    title: str = None
    original_title: str = None
    release_date: str = None
    outline: str = None
    plot: str = None
    tagline: str = None
    genres: list[str] = field(default_factory=list)
    countries: list[str] = field(default_factory=list)
    writers: list[str] = field(default_factory=list)
    directors: list[str] = field(default_factory=list)

    def __post_init__(self):
        self.release_date = (
            parse_date(self.release_date)
            if self.release_date
            else None
        )
        self.genres = [genre.strip() for genre in self.genres]
        self.countries = [country.strip() for country in self.countries]
        self.writers = [writer.strip() for writer in self.writers]
        self.directors = [director.strip() for director in self.directors]


class ImdbError(RuntimeError):
    """Raised when movie detail cannot be retrieved."""


def get_info(title: str):
    """Prepare empty movie detail.

    Returns
    -------
    dict:
        Empty movie detail
    """
    parsed_title = re.match(r"^(?P<title>.+) \((?P<year>\d+?)\)$", title)
    try:
        movie_title = parsed_title["title"]
        movie_year = int(parsed_title["year"])
    except KeyError:
        movie_title = title
        movie_year = None

    imdb_client = IMDb()
    candidates = imdb_client.search_movie(title)
    try:
        movie_overview = next(
            movie
            for movie in candidates
            if (
                movie["title"] == movie_title and
                movie_year in (movie["year"], None)
            )
        )
    except StopIteration as exc:
        print(f"Failed to determine IMDb ID for movie {title}")
        print("Enter it to continue or just hit enter to stop now.")
        backup_imdb_id = input("IMDb ID: ")
        if not backup_imdb_id:
            raise ImdbError(f"Cannot get detail of movie '{title}'") from exc

        movie_id = backup_imdb_id
    else:
        movie_id = movie_overview.movieID

    movie = imdb_client.get_movie(movie_id)
    try:
        raw_release_data = movie["original air date"]
    except KeyError:
        raw_release_data = ""

    try:
        plot = movie["plot"][0].split("::")[0]
    except KeyError:
        plot = None

    return ImdbDetail(
        imdb_id=movie["imdbID"],
        title=movie["localized title"],
        original_title=movie["title"],
        genres=movie["genres"],
        countries=movie["countries"],
        writers=[writer["name"] for writer in movie.get("writer", [])],
        directors=[director["name"] for director in movie.get("director", [])],
        release_date=re.sub(r"\s+\([^)]+?\)$", "", raw_release_data),
        outline=movie.get("plot outline", ""),
        plot=plot,  # TODO: actually a list of plots
        tagline="",  # TODO
    )
