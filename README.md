# Nfo tool - tool for managing nfo files for local movies

CLI utility to scan for movies without `.nfo` files containing information for OSMC media station. It can recognize missing our outdated `.nfo` files and create new ones using information from the movie itself and data loaded from IMDb.


# Table of content

- [Technologies and dependencies](#technologies-and-dependencies)
- [Supported OS](#supported-os)
- [Install](#install)
- [Run](#run)

# Technologies and dependencies

## Technologies

- Python

## Python dependencies

- ffmpeg-python: library for extracting information about media streams
- filelock: lock library to ensure no movie is processed by multiple running `nfo_tool`s
- imdbpy: library for getting data from IMDb site
- yattag: library for saving collected data to XML `.nfo` file.

## External dependencies

- ffmpeg: used by `ffmpeg-python` to retrieve media info


# Supported OS

- Every system that runs python and has `ffmpeg` installed should be able to run `nfo_too` smoothly.
- Tested only on Linux distro.


# Install 

## From pypi.org

Use

```sh
pip install nfo-tools
```

Make sure the installed `nfo_tool` is installed to your `$PATH` or is aliased, symlinked or whatever is your favourite way to make the pip-installed utils executable.

## Build from source

My future self and maybe you might want to build `nfo-tool` yourself. You need two things: `python` and `pip`. Those of us who are lazy might utilize `make`. The harder way is

```sh
pip install build
cd <path-to-nfo-tool>
python -m build
pip install dist/nfo_tools-<version>-py3-none-any.whl
```

With `make`, simply run

```sh
make build
```


# Run

```sh
nfo_tool -h

usage: nfo_tool [-h] {update,remove} [directory]

Tool for creating .nfo files for video library

positional arguments:
  {update,remove}       NFO command
  directory             Directory to search for video files

optional arguments:
  -h, --help            show this help message and exit
```
